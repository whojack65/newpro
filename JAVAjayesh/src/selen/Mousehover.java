package selen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Mousehover {
public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdrive.chrome.driver","C:\\Users\\Admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
	WebDriver driver= new ChromeDriver();
	driver.navigate().to("https://www.hp.com/in-en/home.html");
	driver.manage().window().maximize();
	Thread.sleep(3000);
	
	
	WebElement explore=driver.findElement(By.xpath("//span[text()='Explore']"));
	WebElement laptop=driver.findElement(By.xpath("(//label[text()='Laptops'])[2]"));
	Actions act=new Actions(driver);
	act.moveToElement(explore).perform();
	Thread.sleep(2000);
	act.moveToElement(laptop).click().perform();
	//act.moveToElement(explore).moveToElement(laptop).click().build().perform();
	act.dragAndDrop(explore, laptop).build().perform();
}
}
